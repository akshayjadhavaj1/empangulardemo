import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmpListComponent } from './emp-list/emp-list.component';
import { EmpAddEditComponent } from './emp-add-edit/emp-add-edit.component';
import { EmpPipe } from './emp.pipe';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: EmpListComponent },
  { path: 'add', component: EmpAddEditComponent },  
  { path: 'edit/:firstName', component: EmpAddEditComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    EmpListComponent,
    EmpAddEditComponent,
    EmpPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes) 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
