import { Component, OnInit } from '@angular/core';
import { EmpService } from './../emp.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.scss']
})
export class EmpListComponent implements OnInit {
  empList$: Observable<any>;
  constructor(private empService: EmpService, private router: Router) { }

  ngOnInit() {
    debugger;
    this.getEmpList();
  }

  getEmpList() {
    this.empList$ = this.empService.getEmpList();
  }

  edit(emp) {
    this.router.navigate(['edit', emp.firstName]);
  }
}
