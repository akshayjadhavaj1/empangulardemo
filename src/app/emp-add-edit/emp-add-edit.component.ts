import { Component, OnInit } from '@angular/core';
import { Employee } from './emp.model';
import { EmpService } from './../emp.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-emp-add-edit',
  templateUrl: './emp-add-edit.component.html',
  styleUrls: ['./emp-add-edit.component.scss']
})
export class EmpAddEditComponent implements OnInit {
  isUpdate: boolean;
  employee = {
    "firstName": '',
    "lastName": '',
    "email": ''
  };
  constructor(private empService: EmpService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    debugger;
    this.route.params.subscribe(params => {
      this.empService.getEmpList().subscribe(empList => {
        const emp = empList.find(emp => emp.firstName.toLowerCase() === params.firstName.toLowerCase());
        if (emp) {
          this.isUpdate = true;
          this.employee = emp;
        }
      })
    });
  }

  addEmp() {
    this.empService.addNewEmp(this.employee);
  }

  update(emp) {
    debugger;
    this.empService.updateEmp(emp).subscribe(value => {
      this.router.navigate(['']);
    });
  }

}
