import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  empList: any = [];
  constructor() { }

  getEmpList() {
    return of(this.empList);
  }

  addNewEmp(emp: any) {
    this.empList.push(emp);
  }

  updateEmp(updatedEmp) {
    const emp = this.empList.find(emp => emp.firstName.toLowerCase() === updatedEmp.firstName.toLowerCase());
    if (emp) {
      this.empList.splice(this.empList.indexOf(emp), 1, updatedEmp);
    }
    return of(this.empList);
  }
}
